package com.sashjakk.giphy.explorer.internal

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText

const val tag = "GIPHY"

fun debug(vararg args: Any?) = Log.d(tag, args.joinToString())
fun error(vararg args: Any?) = Log.e(tag, args.joinToString())
fun info(vararg args: Any?) = Log.i(tag, args.joinToString())

fun EditText.onTextChanged(consumer: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            val inputTextValue = s?.toString() ?: ""
            consumer(inputTextValue)
        }

        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    })
}
