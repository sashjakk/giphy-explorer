package com.sashjakk.giphy.explorer.components

import com.sashjakk.giphy.explorer.App
import com.sashjakk.giphy.explorer.modules.ApiModule
import com.sashjakk.giphy.explorer.modules.DataSourceModule
import com.sashjakk.giphy.explorer.modules.MainActivityModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    ApiModule::class,
    MainActivityModule::class,
    DataSourceModule::class
])
interface AppComponent {
    fun inject(app: App)
}