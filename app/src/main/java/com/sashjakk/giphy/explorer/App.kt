package com.sashjakk.giphy.explorer

import android.app.Activity
import android.app.Application
import com.sashjakk.giphy.explorer.components.DaggerAppComponent
import com.sashjakk.giphy.explorer.modules.ApiModule
import com.sashjakk.giphy.explorer.modules.DataSourceModule
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector() = activityInjector

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent
                .builder()
                .apiModule(ApiModule("https://api.giphy.com/v1/"))
                .dataSourceModule(DataSourceModule())
                .build()
                .inject(this)
    }
}