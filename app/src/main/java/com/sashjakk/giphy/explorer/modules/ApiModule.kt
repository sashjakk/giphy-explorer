package com.sashjakk.giphy.explorer.modules

import com.sashjakk.giphy.explorer.api.GiphyAPI
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class ApiModule(private val baseUrl: String) {

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder().build()

    @Provides
    @Singleton
    fun provideApiClient(moshi: Moshi): GiphyAPI {
        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build()

        return retrofit.create(GiphyAPI::class.java)
    }
}