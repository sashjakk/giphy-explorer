package com.sashjakk.giphy.explorer.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.sashjakk.giphy.explorer.GiphyViewModel
import com.sashjakk.giphy.explorer.MainActivity
import com.sashjakk.giphy.explorer.internal.ViewModelFactory
import com.sashjakk.giphy.explorer.internal.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @Binds
    abstract fun bindFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(GiphyViewModel::class)
    abstract fun bindGiphyViewModel(viewModel: GiphyViewModel): ViewModel
}