package com.sashjakk.giphy.explorer.ui

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sashjakk.giphy.explorer.R
import com.sashjakk.giphy.explorer.api.GiphyItem
import com.sashjakk.giphy.explorer.modules.GlideRequests
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_giphy.view.*

class GiphyItemVH(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer

class GiphyItemAdapter(val glide: GlideRequests) : PagedListAdapter<GiphyItem, GiphyItemVH>(DIFF) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GiphyItemVH {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_giphy, parent, false)

        return GiphyItemVH(view)
    }

    override fun onBindViewHolder(holder: GiphyItemVH, position: Int) {
        val target = holder.containerView.giphyItem

        getItem(position)?.let {
            glide.load(it.originalImage.url)
                    .centerCrop()
                    .placeholder(R.drawable.ic_more_horiz_black_24dp)
                    .into(target)
                    .clearOnDetach()
        } ?: glide.clear(target)
    }

    companion object {
        val DIFF = object : DiffUtil.ItemCallback<GiphyItem>() {
            override fun areItemsTheSame(oldItem: GiphyItem, newItem: GiphyItem) =
                    oldItem.id === newItem.id

            override fun areContentsTheSame(oldItem: GiphyItem, newItem: GiphyItem) =
                    oldItem === newItem
        }
    }
}