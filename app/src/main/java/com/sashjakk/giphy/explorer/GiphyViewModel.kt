package com.sashjakk.giphy.explorer

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.sashjakk.giphy.explorer.api.GiphyAPI
import com.sashjakk.giphy.explorer.api.GiphyItem
import javax.inject.Inject

class GiphyViewModel @Inject constructor(
        apiClient: GiphyAPI,
        pagedListConfig: PagedList.Config
) : ViewModel() {

    private var query = ""
    private val queryProvider: QueryProvider = { query }
    private val dataSourceListener = object : DataSourceListener<GiphyItem> {
        override fun onInitialDataLoaded(data: List<GiphyItem>) =
                _totalCount.postValue(data.count())
    }
    private val factory = GiphyDataSourceFactory(apiClient, queryProvider, dataSourceListener)

    private val _totalCount = MutableLiveData<Int>()
    val totalCount: LiveData<Int> = _totalCount

    val giphies = LivePagedListBuilder<Int, GiphyItem>(factory, pagedListConfig)
            .build()

    fun searchItems(newQuery: String) {
        query = newQuery
        giphies.value?.dataSource?.invalidate()
    }
}