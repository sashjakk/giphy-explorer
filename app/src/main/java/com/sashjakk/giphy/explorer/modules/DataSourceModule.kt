package com.sashjakk.giphy.explorer.modules

import android.arch.paging.PagedList
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataSourceModule {

    @Provides
    @Singleton
    fun providesPagedListConfig(): PagedList.Config = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setPageSize(25)
            .setInitialLoadSizeHint(50)
            .setPrefetchDistance(10)
            .build()
}