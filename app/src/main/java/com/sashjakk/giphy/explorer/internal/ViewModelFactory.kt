package com.sashjakk.giphy.explorer.internal

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
class ViewModelFactory @Inject constructor(
        private val moduleMap: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        debug(moduleMap.entries.joinToString { it.key.simpleName })

        val viewModel = moduleMap.entries
                .first { modelClass.isAssignableFrom(it.key) }
                .value

        @Suppress("UNCHECKED_CAST")
        return viewModel.get() as T
    }
}