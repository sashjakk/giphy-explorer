package com.sashjakk.giphy.explorer

import android.arch.paging.DataSource
import android.arch.paging.PositionalDataSource
import android.support.v7.widget.RecyclerView
import com.sashjakk.giphy.explorer.api.GiphyAPI
import com.sashjakk.giphy.explorer.api.GiphyItem
import com.sashjakk.giphy.explorer.api.SearchResponse
import com.sashjakk.giphy.explorer.internal.info
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

typealias QueryProvider = () -> String

class GiphyDataSourceFactory(
        private val giphyAPI: GiphyAPI,
        private val queryProvider: QueryProvider,
        private val dataSourceListener: DataSourceListener<GiphyItem>? = null
) : DataSource.Factory<Int, GiphyItem>() {
    override fun create(): DataSource<Int, GiphyItem> = GiphyDataSource(giphyAPI, queryProvider, dataSourceListener)
}

interface DataSourceListener<T> {
    fun onInitialDataLoaded(data: List<T>) {}
    fun onRangeLoaded(startPosition: Int, loadSize: Int, data: List<T>) {}
}

/** Creates [RecyclerView]'s [DataSource] from Giphy API. */
class GiphyDataSource(
        private val giphyAPI: GiphyAPI,
        private val queryProvider: QueryProvider,
        private val dataSourceListener: DataSourceListener<GiphyItem>? = null
) : PositionalDataSource<GiphyItem>() {

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<GiphyItem>) {
        val query = queryProvider()

        info("Load initial data - query: $query")

        val responseCallback = makeCallback {
            val (_, offset, totalCount) = it.pagination
            dataSourceListener?.onInitialDataLoaded(it.data)
            callback.onResult(it.data, offset, totalCount)
        }

        giphyAPI.search(query, limit = params.pageSize)
                .enqueue(responseCallback)
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<GiphyItem>) {
        val query = queryProvider()

        info("Loading range - offset: ${params.startPosition}, amount: ${params.loadSize}")

        val responseCallback = makeCallback {
            dataSourceListener?.onRangeLoaded(params.startPosition, params.loadSize, it.data)
            callback.onResult(it.data)
        }

        giphyAPI.search(query, limit = params.loadSize, offset = params.startPosition)
                .enqueue(responseCallback)
    }

    private inline fun makeCallback(
            crossinline invoke: (SearchResponse) -> Unit
    ) = object : Callback<SearchResponse> {
        override fun onFailure(call: Call<SearchResponse>, t: Throwable) =
                t.printStackTrace()

        override fun onResponse(call: Call<SearchResponse>, response: Response<SearchResponse>) {
            val searchResponse = response.body()
                    ?: throw IllegalStateException("Empty response body")

            val (page, meta) = searchResponse

            info("Data loaded - " +
                    "status: ${meta.status}, " +
                    "response: ${meta.responseId}, " +
                    "total items: ${page.totalCount}")

            invoke(searchResponse)
        }

    }
}