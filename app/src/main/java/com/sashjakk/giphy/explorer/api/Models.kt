package com.sashjakk.giphy.explorer.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Pagination(
        val count: Int,
        val offset: Int,
        @Json(name = "total_count") val totalCount: Int
)

@JsonClass(generateAdapter = true)
data class Meta(
        val status: Int,
        val msg: String,
        @Json(name = "response_id") val responseId: String
)

@JsonClass(generateAdapter = true)
data class Image(
        val url: String,
        val width: String,
        val height: String
)

@JsonClass(generateAdapter = true)
data class GiphyItem(
        val type: String,
        val id: String,
        val images: Map<String, Map<String, String>>
) {

    val originalImage by lazy {
        imageFromMap(images["original"]
                ?: throw IllegalStateException("Image data is missing"))
    }

    /** Create [Image] object from map with data. */
    private fun imageFromMap(imageRawData: Map<String, String>): Image {
        val url: String by imageRawData
        val width: String by imageRawData
        val height: String by imageRawData

        return Image(url, width, height)
    }
}

@JsonClass(generateAdapter = true)
data class SearchResponse(
        val pagination: Pagination,
        val meta: Meta,
        val data: List<GiphyItem>
)