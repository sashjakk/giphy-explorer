package com.sashjakk.giphy.explorer.api

import com.sashjakk.giphy.explorer.BuildConfig
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GiphyAPI {

    @GET("gifs/search")
    fun search(
            @Query("q") keyword: String,
            @Query("limit") limit: Int = 25,
            @Query("offset") offset: Int = 0,
            @Query("api_key") apiKey: String = BuildConfig.GIPHY_API_KEY
    ): Call<SearchResponse>
}