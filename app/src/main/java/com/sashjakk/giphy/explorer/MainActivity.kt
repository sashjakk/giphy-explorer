package com.sashjakk.giphy.explorer

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.sashjakk.giphy.explorer.internal.debug
import com.sashjakk.giphy.explorer.internal.onTextChanged
import com.sashjakk.giphy.explorer.modules.GlideApp
import com.sashjakk.giphy.explorer.ui.GiphyItemAdapter
import com.sashjakk.giphy.explorer.ui.MarginItemDecoration
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var vmFactory: ViewModelProvider.Factory

    private val viewModel: GiphyViewModel by lazy {
        ViewModelProviders.of(this, vmFactory).get(GiphyViewModel::class.java)
    }

    // live search
    private val debounceInterval = 300L
    private val handler = Handler()
    private var activeRunnable: Runnable? = null

    // empty items info
    private val noItemsViews by lazy { listOf(noItemsImageView, noItemsTextView) }

    // grid
    private val columnCount: Int
        get() = when (resources.configuration.orientation) {
            ORIENTATION_PORTRAIT -> 2
            ORIENTATION_LANDSCAPE -> 3
            else -> throw IllegalStateException("Unknown screen orientation")
        }

    private val giphyAdapter by lazy { GiphyItemAdapter(GlideApp.with(this@MainActivity)) }
    private val itemDecoration by lazy { MarginItemDecoration(16) }
    private val gridLayoutManager by lazy { GridLayoutManager(this@MainActivity, columnCount) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_main)

        setupGiphyGrid()
        setupLiveSearch()
        setupClearSearch()

        debug("view model $viewModel")

        viewModel.totalCount.observe(this, Observer { showEmptyItemsInfo(it == 0) })
        viewModel.giphies.observe(this, Observer { giphyAdapter.submitList(it) })
    }

    private fun setupLiveSearch() = inputEditText.onTextChanged { query ->
        activeRunnable?.let { handler.removeCallbacks(it) }

        activeRunnable = Runnable { viewModel.searchItems(query) }
        handler.postDelayed(activeRunnable, debounceInterval)
    }

    private fun setupClearSearch() = clearSearchButton.setOnClickListener {
        viewModel.searchItems("")
        inputEditText.setText("")
    }

    private fun setupGiphyGrid() = giphyGrid.apply {
        layoutManager = gridLayoutManager
        adapter = giphyAdapter
        addItemDecoration(itemDecoration)
    }

    private fun showEmptyItemsInfo(show: Boolean) =
            noItemsViews.forEach { it.visibility = if (show) View.VISIBLE else View.GONE }
}

