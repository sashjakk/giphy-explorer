# Giphy Explorer

Simple Giphy client which allows to search gifs via Giphy REST API while typing.

![App preview](https://bitbucket.org/sashjakk/giphy-explorer/raw/master/giphy-explorer.gif)

## Build

1. Create gradle.properties in project app folder
2. Provide your Giphy API key

or invoke this in terminal:

```
echo giphyApiKey=YOUR-GIPHY-API-KEY > app/gradle.properties
```

3. Build via gradle

```
./gradlew clean build
```

## Libraries used

- Retrofit for REST requests
- Moshi for reflectionless JSON parsing
- Glide to fetch images and display GIF
- ViewModel from Arch components to persist orientation changes
- Paging Library to allow endless scrolling
- Dagger2 for depency injections
